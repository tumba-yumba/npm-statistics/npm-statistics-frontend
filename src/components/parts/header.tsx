import {Button, Spinner} from "@skbkontur/react-ui";
import {useCollectorRequestMutation, useGetUpdateStatusQuery} from "../../store/api";

export function Header(){
    const {data: status, refetch} = useGetUpdateStatusQuery(undefined);
    const [update, {isLoading}] = useCollectorRequestMutation();
    const handleClick = () => {
        update(undefined).then(() => refetch());
    }
    return (
        <div className="npm-header">
            <h1 className="npm-header_logo-name">
                <span className='logo-name_npm-name'>NPM</span>
                .
                <span>Statistics</span>
            </h1>
            <div className="npm-header__action-block">
                {isLoading && <Spinner/>}
                {!isLoading && status && <div className="last-update-info">Дата обновления: <br/> {(new Date(status.last_update_time + 'Z')).toLocaleString('ru')} </div>}
                <Button use="primary" disabled={!status?.update_is_allowed} onClick={handleClick}> Обновить </Button>
            </div>
        </div>
    )
}