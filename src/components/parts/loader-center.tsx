import {Spinner} from "@skbkontur/react-ui";

export function LoaderCenter() {
    return (
        <div className="loader-container">
            <Spinner type="big"/>
        </div>
    )
}