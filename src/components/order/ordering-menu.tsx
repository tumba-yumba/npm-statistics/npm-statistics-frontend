import {Button, DropdownMenu, MenuHeader, MenuItem, MenuSeparator} from "@skbkontur/react-ui";
import {ArrowChevronDown, ArrowChevronUp, Filter} from "@skbkontur/react-icons";
import {Ordering} from "../../types/utils/ordering";

interface OrderingMenuProps {
    onOrderChange: (newVal: Ordering) => void
}

export function OrderingMenu({onOrderChange}:OrderingMenuProps){
    return (
        <DropdownMenu caption={<Button size="medium" borderless><Filter/></Button>}>
            <MenuHeader>Сортировка</MenuHeader>
            <MenuSeparator />
            <MenuItem icon={<ArrowChevronUp/>}  onClick={() => onOrderChange(Ordering.nameAsc)}>По названию</MenuItem>
            <MenuItem icon={<ArrowChevronDown/>}  onClick={() => onOrderChange(Ordering.nameDesc)}>По названию</MenuItem>
            {/*<MenuSeparator />*/}
            {/*<MenuItem icon={<ArrowChevronUp/>} onClick={() => onOrderChange(Ordering.timeAsc)}>По дате изменения</MenuItem>*/}
            {/*<MenuItem icon={<ArrowChevronDown/>} onClick={() => onOrderChange(Ordering.timeDesc)}>По дате изменения</MenuItem>*/}
        </DropdownMenu>
    )
}