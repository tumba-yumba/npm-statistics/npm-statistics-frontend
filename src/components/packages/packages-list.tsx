import {Packet} from "../../types/packets/packet";
import {FormEvent, useCallback, useMemo, useState} from "react";
import {useGetDependenciesQuery} from "../../store/api";
import {PAGE_SIZE} from "../../config";
import {LoaderCenter} from "../parts/loader-center";
import useInfiniteScroll from "react-infinite-scroll-hook";
import {Input} from "@skbkontur/react-ui";
import SearchIcon from '@skbkontur/react-icons/Search';

interface PackagesListProps {
    projectId: number,

    onClickPackage: (packet: Packet) => void

    hidden?: boolean
}
export function PackagesList({projectId, onClickPackage, hidden}:PackagesListProps){
    const [activePage, setActivePage] = useState(0);
    const [searchParam, setSearchParams] = useState("");
    const [previousPackets, setPrevios] = useState<Array<Packet>>([]);
    const {data: dependencies, isLoading, error} = useGetDependenciesQuery({project_id: projectId, package_name: searchParam, page: activePage, per_page: 3 * PAGE_SIZE});
    const updatePage = useCallback(() => {
        console.log("Update page")
        if (dependencies?.items){
            setPrevios((prev) => prev.concat(dependencies.items))
        }
        setActivePage((last) => last + 1);
    }, [setActivePage, setPrevios, dependencies])
    const totalItems = useMemo(() => {
        if (dependencies?.items){
            return previousPackets.concat(dependencies.items)
        }
        return previousPackets;
    }, [dependencies]);
    const totalPages = Math.ceil((dependencies?.total_count ?? 0) / (3 * PAGE_SIZE));
    const [sentryRef, { rootRef }] = useInfiniteScroll({
        loading: isLoading,
        hasNextPage: activePage < totalPages,
        onLoadMore: updatePage,
        disabled: !!error,
        rootMargin: '0px 0px 400px 0px',
    });
    const handleSearchSubmit = useCallback((event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        setSearchParams(data.get('searcher') as string);
        setActivePage(0);
        setPrevios([]);
    }, [setActivePage, setPrevios])
    return (
        <>
        {isLoading && !hidden && totalItems.length === 0 && <LoaderCenter/>}
        <div className="packages-list" style={{display: hidden ? 'none' : 'block'}}>
            <form onSubmit={handleSearchSubmit}><Input type="search" name="searcher" placeholder="Название пакета: например, chalk" width="100%" leftIcon={<SearchIcon />}/></form>
            <h3 className="packages-modal__help">Список используемых пакетов</h3>
            <div className="packages-list__scroller" ref={rootRef}>
                    {totalItems.map((dep) => (
                        <div className="packages-list__package"  key={dep.name}>
                            <div className="package__name package_click" onClick={() => onClickPackage(dep)}>
                                {dep.name}
                            </div>
                            <div className="package__version package_click" onClick={() => onClickPackage(dep)}>
                                {dep.version}
                            </div>
                        </div>
                    ))}
                    <hr ref={sentryRef}/>
            </div>
        </div>
        </>
    )
}