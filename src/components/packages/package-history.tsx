import {useCallback, useMemo, useState} from "react";
import {Button} from "@skbkontur/react-ui";
import {ArrowLeftIcon} from "@skbkontur/react-ui/components/Button/ArrowLeftIcon";
import {PacketHistoryItem} from "../../types/packets/packet-history-item";
import {useGetPackageHistoryQuery} from "../../store/api";
import {PAGE_SIZE} from "../../config";
import useInfiniteScroll from "react-infinite-scroll-hook";
import {CalendarIcon} from "@skbkontur/react-ui/internal/icons/16px";
import {Clock} from "@skbkontur/react-icons";
import {LoaderCenter} from "../parts/loader-center";

interface PackageHistoryProps {
    packetName: string,
    projectId: number,
    onBackClick?: () => void
}

export function PackageHistory({packetName, projectId, onBackClick}: PackageHistoryProps) {
    const [activePage, setActivePage] = useState(0);
    const [previous, setPrevios] = useState<Array<PacketHistoryItem>>([]);
    const {data, isLoading, error} = useGetPackageHistoryQuery({project_id: projectId, package_name: packetName, page: activePage, per_page: 3 * PAGE_SIZE});
    const updatePage = useCallback(() => {
        console.log("Update page")
        if (data?.items){
            setPrevios((prev) => prev.concat(data.items))
        }
        setActivePage((last) => last + 1);
    }, [setActivePage, setPrevios, data])
    const totalItems = useMemo(() => {
        if (data?.items){
            return previous.concat(data.items)
        }
        return previous;
    }, [data]);
    const totalPages = Math.ceil((data?.total_count ?? 0) / (3 * PAGE_SIZE));
    const [sentryRef, { rootRef }] = useInfiniteScroll({
        loading: isLoading,
        hasNextPage: activePage < totalPages,
        onLoadMore: updatePage,
        disabled: !!error,
        rootMargin: '0px 0px 400px 0px',
    });
    return (
        <>
            <h4 className="package__title"> {packetName}</h4>
            <Button onClick={() => onBackClick ? onBackClick() : 0} className="back_button" use="success" icon={<ArrowLeftIcon/>} size="small">Назад</Button>
            <h3 className="packages-modal__help">История изменений пакета</h3>
            <div className="packages-list">
                {isLoading && totalItems.length === 0 && <LoaderCenter/>}
                <div className="packages-list__scroller" ref={rootRef}>
                    {totalItems.map((history) => (
                        <div className="packages-list__package" key={history.time}>
                            <div className="package__history">
                                <p>
                                   <span className="green-icon"><CalendarIcon/></span> {history.timeObj.toLocaleDateString('ru')}
                                </p>
                                <p>
                                    <span className="green-icon"><Clock/></span>  {history.timeObj.toLocaleTimeString('ru')}
                                </p>
                            </div>
                            <div className="package__version package__version_history">
                                {history.package_version}
                            </div>
                        </div>
                    ))}
                    <hr ref={sentryRef}/>
                </div>
            </div>
        </>
    )
}