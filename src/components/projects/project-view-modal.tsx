import {Project} from "../../types/projects/project";
import {Modal} from "@skbkontur/react-ui";
import {useCallback, useState} from "react";
import {Packet} from "../../types/packets/packet";
import {PackagesList} from "../packages/packages-list";
import {PackageHistory} from "../packages/package-history";

interface ProjectViewModalProps {
    project: Project,
    packet?: Packet,
    onClose: () => void
}

export function ProjectViewModal({project, onClose, packet}: ProjectViewModalProps) {
    const [selectedPackage, setPackage] = useState<Packet | null | undefined>(packet);
    const handlePacketClick = useCallback((packet: Packet) => setPackage(packet), []);

    return (
        <Modal onClose={onClose}>
            <Modal.Header>{project.name}</Modal.Header>
            <Modal.Body>
              <PackagesList projectId={project.id} onClickPackage={handlePacketClick} hidden={!!selectedPackage}/>
              {selectedPackage && <PackageHistory projectId={project.id} packetName={selectedPackage.name} onBackClick={() => setPackage(null)}/>}
            </Modal.Body>
        </Modal>
    )
}