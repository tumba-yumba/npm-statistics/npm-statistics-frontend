import {Project} from "../../types/projects/project";
import {Button, Link} from "@skbkontur/react-ui";
import {
    ArrowShapeDRadiusUpLeftIcon16Light
} from "@skbkontur/react-ui/internal/icons2022/ArrowShapeDRadiusUpLeftIcon/ArrowShapeDRadiusUpLeftIcon16Light";

interface ProjectItemProps {
    project: Project,
    onClick?: () => void,
}
export function ProjectItem({project, onClick}: ProjectItemProps){
    return (
        <div className="value-item">
            <Button use="link"  onClick={onClick}>
                {project.name}
            </Button>
            <div className="project-info">
                {project.package && project.package.version && <div className="package__version package_click" onClick={onClick}>
                    <span className="packet-name__info">{project.package.name}</span>
                    {project.package.version}
                </div>}
                {project.repository_url && <Link use={"grayed"} href={project.repository_url} target="_blank" icon={<ArrowShapeDRadiusUpLeftIcon16Light/>}/>}

            </div>
        </div>
    )
}