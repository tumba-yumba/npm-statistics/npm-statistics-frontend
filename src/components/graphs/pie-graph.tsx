import {ResponsivePie} from "@nivo/pie";
import {useGetPackageStatisticsQuery} from "../../store/api";
import {Spinner} from "@skbkontur/react-ui";
import {useMemo} from "react";
import {formatNumber} from "../../utils";

interface PieGraphProps {
    projectName: string,
    versionFilter: string
}
export function PieGraph({projectName, versionFilter}:PieGraphProps){
    const {data, isLoading} = useGetPackageStatisticsQuery({name: projectName, version: versionFilter});
    const total = useMemo(() => data?.reduce((sum, item) => sum + item.value, 0) ?? 0, [data])
    return (
        <>
        {isLoading && <Spinner/>}
        {!isLoading && data && <ResponsivePie
            data={data}
            margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
            innerRadius={0.5}
            padAngle={0.7}
            cornerRadius={3}
            valueFormat={(number) => `${number} (${formatNumber((number / total) * 100)}%)`}
            activeOuterRadiusOffset={8}
            borderColor={{
                from: 'color',
                modifiers: [
                    [
                        'darker',
                        0.2
                    ]
                ]
            }}
            arcLinkLabelsSkipAngle={10}
            arcLinkLabelsTextColor="#333333"
            arcLinkLabelsThickness={2}
            arcLinkLabelsColor={{ from: 'color' }}
            arcLabelsSkipAngle={10}
            arcLabelsTextColor={{
                from: 'color',
                modifiers: [
                    [
                        'darker',
                        2
                    ]
                ]
            }}
            legends={[
                {
                    anchor: 'bottom',
                    direction: 'row',
                    justify: false,
                    translateX: 0,
                    translateY: 56,
                    itemsSpacing: 0,
                    itemWidth: 100,
                    itemHeight: 18,
                    itemTextColor: '#999',
                    itemDirection: 'left-to-right',
                    itemOpacity: 1,
                    symbolSize: 18,
                    symbolShape: 'circle',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemTextColor: '#000'
                            }
                        }
                    ]
                }
            ]}
        />}
        </>
    )
}