import {PieGraph} from "./pie-graph";

interface GraphsTabProps {
    graphicsFor?: string

    versionFilter: string
}

export function GraphsTab({graphicsFor, versionFilter}: GraphsTabProps) {
    return (
        <div className="graphics">

            {!graphicsFor && <div className="graphics__placeholder">
                <p className="graphics__placeholder__text">Выберите пакет для отображения графика</p>
            </div>}
            {graphicsFor && <>
                <div className="graph">
                    <PieGraph projectName={graphicsFor} versionFilter={versionFilter}/>
                </div>
            </>}
        </div>
    )
}