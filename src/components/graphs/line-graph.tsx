import { ResponsiveLine } from '@nivo/line'
const data = [
    {
        "id": "japan",
        "color": "hsl(321, 70%, 50%)",
        "data": [
            {
                "x": "plane",
                "y": 32
            },
            {
                "x": "helicopter",
                "y": 15
            },
            {
                "x": "boat",
                "y": 172
            },
            {
                "x": "train",
                "y": 235
            },
            {
                "x": "subway",
                "y": 194
            },
            {
                "x": "bus",
                "y": 128
            },
            {
                "x": "car",
                "y": 197
            },
            {
                "x": "moto",
                "y": 206
            },
            {
                "x": "bicycle",
                "y": 255
            },
            {
                "x": "horse",
                "y": 66
            },
            {
                "x": "skateboard",
                "y": 243
            },
            {
                "x": "others",
                "y": 59
            }
        ]
    },
    {
        "id": "france",
        "color": "hsl(271, 70%, 50%)",
        "data": [
            {
                "x": "plane",
                "y": 268
            },
            {
                "x": "helicopter",
                "y": 77
            },
            {
                "x": "boat",
                "y": 119
            },
            {
                "x": "train",
                "y": 229
            },
            {
                "x": "subway",
                "y": 50
            },
            {
                "x": "bus",
                "y": 141
            },
            {
                "x": "car",
                "y": 176
            },
            {
                "x": "moto",
                "y": 5
            },
            {
                "x": "bicycle",
                "y": 135
            },
            {
                "x": "horse",
                "y": 8
            },
            {
                "x": "skateboard",
                "y": 137
            },
            {
                "x": "others",
                "y": 272
            }
        ]
    },
    {
        "id": "us",
        "color": "hsl(224, 70%, 50%)",
        "data": [
            {
                "x": "plane",
                "y": 271
            },
            {
                "x": "helicopter",
                "y": 297
            },
            {
                "x": "boat",
                "y": 76
            },
            {
                "x": "train",
                "y": 149
            },
            {
                "x": "subway",
                "y": 177
            },
            {
                "x": "bus",
                "y": 110
            },
            {
                "x": "car",
                "y": 265
            },
            {
                "x": "moto",
                "y": 271
            },
            {
                "x": "bicycle",
                "y": 184
            },
            {
                "x": "horse",
                "y": 29
            },
            {
                "x": "skateboard",
                "y": 15
            },
            {
                "x": "others",
                "y": 77
            }
        ]
    },
    {
        "id": "germany",
        "color": "hsl(338, 70%, 50%)",
        "data": [
            {
                "x": "plane",
                "y": 26
            },
            {
                "x": "helicopter",
                "y": 244
            },
            {
                "x": "boat",
                "y": 149
            },
            {
                "x": "train",
                "y": 257
            },
            {
                "x": "subway",
                "y": 37
            },
            {
                "x": "bus",
                "y": 188
            },
            {
                "x": "car",
                "y": 198
            },
            {
                "x": "moto",
                "y": 295
            },
            {
                "x": "bicycle",
                "y": 290
            },
            {
                "x": "horse",
                "y": 170
            },
            {
                "x": "skateboard",
                "y": 220
            },
            {
                "x": "others",
                "y": 284
            }
        ]
    },
    {
        "id": "norway",
        "color": "hsl(23, 70%, 50%)",
        "data": [
            {
                "x": "plane",
                "y": 253
            },
            {
                "x": "helicopter",
                "y": 4
            },
            {
                "x": "boat",
                "y": 82
            },
            {
                "x": "train",
                "y": 155
            },
            {
                "x": "subway",
                "y": 85
            },
            {
                "x": "bus",
                "y": 81
            },
            {
                "x": "car",
                "y": 21
            },
            {
                "x": "moto",
                "y": 76
            },
            {
                "x": "bicycle",
                "y": 117
            },
            {
                "x": "horse",
                "y": 269
            },
            {
                "x": "skateboard",
                "y": 169
            },
            {
                "x": "others",
                "y": 18
            }
        ]
    }
]
export function LineGraph(){
    return (
        <ResponsiveLine
            data={data}
            margin={{ top: 50, right: 50, bottom: 110, left: 60 }}
            xScale={{ type: 'point' }}
            yScale={{
                type: 'linear',
                min: 'auto',
                max: 'auto',
                stacked: true,
                reverse: false
            }}
            yFormat=" >-.2f"
            axisTop={null}
            axisRight={null}
            axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'transportation',
                legendOffset: 36,
                legendPosition: 'middle'
            }}
            axisLeft={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'count',
                legendOffset: -40,
                legendPosition: 'middle'
            }}
            pointSize={10}
            pointColor={{ theme: 'background' }}
            pointBorderWidth={2}
            pointBorderColor={{ from: 'serieColor' }}
            pointLabelYOffset={-12}
            useMesh={true}
            legends={[
                {
                    anchor: 'bottom-left',
                    direction: 'row',
                    justify: false,
                    translateX: 0,
                    translateY: 60,
                    itemWidth: 80,
                    itemHeight: 10,
                    itemsSpacing: 0,
                    symbolSize: 15,
                    symbolShape: 'circle',
                    itemDirection: 'left-to-right',
                    itemTextColor: '#777',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemBackground: 'rgba(0, 0, 0, .03)',
                                itemOpacity: 1
                            }
                        }
                    ]
                }
            ]}
        />
    )
}