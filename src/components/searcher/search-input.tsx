import {Button, ComboBox, ComboBoxItem, Input} from "@skbkontur/react-ui";
import {useCallback, useState} from "react";
import {SearchData} from "../../types/projects/search-data";
import {OrderingMenu} from "../order/ordering-menu";
import {Ordering} from "../../types/utils/ordering";
import {projectsApi} from "../../store/api";
import {useAppDispatch} from "../../store/hooks";

interface SearchInputProps{
    onStartSearch: (value: SearchData) => void
    onOrderingChange: (value: Ordering) => void
}

export function SearchInput({onStartSearch, onOrderingChange}: SearchInputProps) {
    const [searchQuery, setSearchQuery] = useState<ComboBoxItem>({value: "", label: ""});
    const [inputValue, setInputValue] = useState<string>("");
    const [versionQuery, setVersionQuery] = useState<string>("");
    const dispatch = useAppDispatch();
    const getItems = useCallback(async (q: string) => {
        const {data} = await dispatch(projectsApi.endpoints.getPackagesHelper.initiate(q));
        if (data)
            return data;
        return [];
    }, [dispatch]);
    const handleValueChange = useCallback((item: ComboBoxItem) => {
        setSearchQuery(item);
        setInputValue(item.value);
    }, []);
    const handleInputChange = useCallback((item: string) => setInputValue(item), [setInputValue]);
    const handleInputBlur = useCallback(() => setSearchQuery({value: inputValue, label: inputValue}), [inputValue]);
    return (
        <div className="search-input">
            <OrderingMenu onOrderChange={onOrderingChange}/>
            <ComboBox
                getItems={getItems}
                size="medium"
                onValueChange={handleValueChange}
                onInputValueChange={handleInputChange}
                onBlur={handleInputBlur}
                className="search-input__input"
                placeholder="Название пакета: например, chalk"
                width="100%"
                borderless
                value={searchQuery}
            />

            <Input
                placeholder=">=17.2.0 || 12.2.x"
                size="medium"
                value={versionQuery}
                className="search-input__version"
                borderless
                onChange={(event) => setVersionQuery(event.target.value)}
            />
            <Button use="success" className="search-input__button" onClick={() => onStartSearch({name: searchQuery.value, version: versionQuery})} size="medium">
                Найти
            </Button>
        </div>
    )
}