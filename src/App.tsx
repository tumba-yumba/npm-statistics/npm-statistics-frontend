import './App.css'
import {Header} from "./components/parts/header";
import {SearchInput} from "./components/searcher/search-input";
import {Project} from "./types/projects/project";
import {ProjectItem} from "./components/projects/project-item";
import {GraphsTab} from "./components/graphs/graphs-tab";
import {useCallback, useState} from "react";
import {ProjectViewModal} from "./components/projects/project-view-modal";
import {useGetProjectsByFilterQuery} from "./store/api";
import {Paging} from "@skbkontur/react-ui";
import {SearchData} from "./types/projects/search-data";
import {PAGE_SIZE} from "./config";
import {LoaderCenter} from "./components/parts/loader-center";
import {Ordering} from "./types/utils/ordering";

function App() {
    const [selectedProject, setSelectedProject] = useState<Project | null>(null);
    const [activePage, setActivePage] = useState(1);
    const [searchQuery, setSearchQuery] = useState<SearchData>({name:"", version:""});
    const [ordering, setOrdering] = useState<Ordering>(Ordering.nameAsc);
    const  { data, error, isLoading } = useGetProjectsByFilterQuery({page: activePage - 1, per_page: PAGE_SIZE, ordering, ...searchQuery})
    const handleCloseModal = useCallback(() => setSelectedProject(null), []);
    const handleChangeOrdering = useCallback((newVal: Ordering) => setOrdering(newVal), []);
    // @ts-ignore
    return (
        <>
          <Header/>
            <div className="search-container">
            <SearchInput onStartSearch={setSearchQuery} onOrderingChange={handleChangeOrdering}/>
                <p className="total-repos">
                    Всего проектов: <span className="total-repos__number">{data?.total_count ?? 0}</span>
                </p>
            </div>
          <div className='content'>
              <div className='content__part content__part_packages'>
                  {isLoading && <LoaderCenter/>}
                  {error && <div className="error-block">
                      Ошибка: &nbsp;
                      {(error as any).data?.detail || JSON.stringify(error)}
                  </div>}
                  {!isLoading && data && data.items.length === 0 && <h4 className="loader-container">
                      Ничего не найдено
                  </h4>}
                  <div className='projects-list'>
                      {data && data.items.map((item) => <ProjectItem key={item.id} project={item} onClick={() => setSelectedProject(item)}/>)}
                      {data && data.items.length > 0 && (data?.total_count ?? 0) > PAGE_SIZE && <Paging
                          className="projects-list__pagination"
                          onPageChange={(activePage) => setActivePage(activePage)}
                          activePage={activePage}
                          pagesCount={Math.ceil((data?.total_count ?? 0) / PAGE_SIZE)}
                      />}
                  </div>
              </div>
                <div className='content__part content__part_graph'>
                    <GraphsTab graphicsFor={searchQuery.name} versionFilter={searchQuery.version}/>
                </div>
          </div>
            {selectedProject && <ProjectViewModal project={selectedProject} packet={selectedProject.package?.version ? selectedProject.package : undefined} onClose={handleCloseModal}/>}
        </>
    )
}

export default App
