import {Packet} from "../packets/packet";

export interface Project {
    id: number,
    name: string,
    repository_url: string,
    package?: Packet
}