export interface UpdateStatus {
    last_update_time: string,
    update_is_allowed: boolean
}
