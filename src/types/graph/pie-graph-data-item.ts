export interface PieGraphDataItem {
    label: string,
    value: number,
    id: string
}