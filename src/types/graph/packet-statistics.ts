import {StatItem} from "./stat-item";

export interface PacketStatistics {
    stats: Array<StatItem>
}