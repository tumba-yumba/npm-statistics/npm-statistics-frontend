export interface PacketHistoryItem {
    time: string,
    package_version: string,

    timeObj: Date
}