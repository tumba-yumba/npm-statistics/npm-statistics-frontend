export interface Packet {
    name: string,
    version: string,
}