export interface PagingResponse<T> {
    items: Array<T>,
    total_count: number,
}