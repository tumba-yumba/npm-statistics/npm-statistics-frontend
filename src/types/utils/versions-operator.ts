export enum VersionsOperator {
    Equal = '=',
    NotEqual = '!=',
    GreaterOrEqual = '>=',
    LessOrEqual = '<=',
    Less = '<',
    More = '>',
}