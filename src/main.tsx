import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import {THEME_2022, ThemeContext, ThemeFactory} from "@skbkontur/react-ui";
import {Provider} from "react-redux";
import {store} from "./store/store";
const npmTheme = ThemeFactory.create({
    btnSuccessBg: '#00BEA2',
    btnSuccessActiveBg: '#007559',
    btnSuccessHoverBg: '#007564',
    btnSuccessBorderColor: 'transparent',
    btnSuccessHoverBorderColor: 'transparent',
    inputBorderRadiusMedium: '10px',
}, THEME_2022);

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <Provider store={store}>
          <ThemeContext.Provider value={npmTheme}>
            <App />
          </ThemeContext.Provider>
      </Provider>
  </React.StrictMode>,
)
