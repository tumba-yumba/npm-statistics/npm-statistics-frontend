import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import {AppDispatch, RootState} from "./store";
import {useCallback, useEffect, useMemo, useState} from "react";
import {PagingResponse} from "../types/utils/paging-response";
import {PAGE_SIZE} from "../config";
import useInfiniteScroll from "react-infinite-scroll-hook";

export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export function useDebounce<T>(value: T, delay: number): T {
    const [debouncedValue, setDebouncedValue] = useState<T>(value);

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        return () => {
            clearTimeout(handler);
        };
    }, [value, delay]);

    return debouncedValue;
}

export function useInfiniteScrollOnData<T>(data: PagingResponse<T> | undefined, isLoading: boolean, disabled: boolean)  {
    const [activePage, setActivePage] = useState(0);
    const [previous, setPrevios] = useState<Array<T>>([]);
    const updatePage = useCallback(() => {
        if (data?.items){
            setPrevios((prev) => prev.concat(data.items))
        }
        setActivePage((last) => last + 1);
    }, [setActivePage, setPrevios, data])
    const totalItems = useMemo(() => {
        if (data?.items){
            return previous.concat(data.items)
        }
        return previous;
    }, [data]);
    const totalPages = Math.ceil((data?.total_count ?? 0) / (3 * PAGE_SIZE));
    const [sentryRef, {rootRef}] = useInfiniteScroll({
        loading: isLoading,
        hasNextPage: activePage < totalPages,
        onLoadMore: updatePage,
        disabled: disabled,
        rootMargin: '0px 0px 400px 0px',
    });
    const clear = useCallback(() => {
        setActivePage(0);
        setPrevios([])
    }, [])
    return {sentryRef ,totalItems, activePage, rootRef, clear}
}