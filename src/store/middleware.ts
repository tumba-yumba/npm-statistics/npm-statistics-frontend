import { isRejectedWithValue } from '@reduxjs/toolkit'
import type { Middleware } from '@reduxjs/toolkit'
import { Toast } from '@skbkontur/react-ui';
export const rtkQueryErrorLogger: Middleware =
    () => (next) => (action) => {
        // RTK Query uses `createAsyncThunk` from redux-toolkit under the hood, so we're able to utilize these matchers!
        if (isRejectedWithValue(action)) {
            console.warn('We got a rejected action!')
            Toast.push("Error on request")
        }

        return next(action)
    }