import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {PagingResponse} from "../types/utils/paging-response";
import {Project} from "../types/projects/project";
import {Page} from "../types/utils/page";
import {Packet} from "../types/packets/packet";
import {SearchData} from "../types/projects/search-data";
import {API_URL} from "../config";
import {PacketHelper} from "../types/responce/packet-helper";
import {Ordering} from "../types/utils/ordering";
import {ComboBoxItem} from "@skbkontur/react-ui";
import {UpdateStatus} from "../types/responce/update-status";
import {SuccessResponse} from "../types/responce/success-response";
import {ProjectPackage} from "../types/projects/project-package";
import {PacketHistoryItem} from "../types/packets/packet-history-item";
import {PacketStatistics} from "../types/graph/packet-statistics";
import {PieGraphDataItem} from "../types/graph/pie-graph-data-item";



export const projectsApi = createApi({
    reducerPath: 'projectsApi',
    baseQuery: fetchBaseQuery({ baseUrl: API_URL }),
    endpoints: (builder) => ({
        getPackagesHelper: builder.query<ComboBoxItem[], string>({
            query: (name) => ({url: `packages`, params: {page: 0, per_page: 15, name}}),
            transformResponse: (response: PagingResponse<PacketHelper>) => response.items.map((item) => ({label: item.name, value: item.name})),
        }),
        getDependencies: builder.query<PagingResponse<Packet>, Page & ProjectPackage>({
            query: (params) => ({url: `projects/${params.project_id}/packages`, params})
        }),
        getProjectsByFilter: builder.query<PagingResponse<Project>, Page & SearchData & {ordering: Ordering}>({
            query: (params) => ({
                url: 'projects',
                params: {
                    page: params.page,
                    per_page: params.per_page,
                    package_version: params.version,
                    package_name: params.name,
                    order_by: params.ordering
                },
            })
        }),
        getUpdateStatus: builder.query<UpdateStatus, undefined>({
            query: () => ({url: `collector/status`}),
        }),
        collectorRequest: builder.mutation<SuccessResponse, undefined>({
            query: () => ({url: `collector/update`, method: 'POST',}),
        }),
        getPackageHistory: builder.query<PagingResponse<PacketHistoryItem>, Page & ProjectPackage>({
            query: (params) => ({url: `projects/${params.project_id}/history`, params}),
            transformResponse: (response: PagingResponse<PacketHistoryItem>) => {
                response.items.forEach((item) => item.timeObj = new Date(item.time))
                return response;
            },
        }),
        getPackageStatistics: builder.query<Array<PieGraphDataItem>, SearchData>({
            query: (searchData) => ({url: `packages/stats/projects_count`, params: {package_name: searchData.name, package_version: searchData.version}}),
            transformResponse: (response: PacketStatistics) => {
                return response.stats.map((item) => ({label: item.package_version, value: item.projects_count, id: item.package_version}));
            },
        })
    }),
})

export const { useGetUpdateStatusQuery, useGetPackageStatisticsQuery, useGetPackageHistoryQuery, useCollectorRequestMutation, useGetDependenciesQuery, useGetProjectsByFilterQuery } = projectsApi