import { configureStore } from '@reduxjs/toolkit'
import {projectsApi} from "./api";
import {rtkQueryErrorLogger} from "./middleware";

export const store = configureStore({
    reducer: {
        [projectsApi.reducerPath]: projectsApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(projectsApi.middleware).concat(rtkQueryErrorLogger),
})


export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
